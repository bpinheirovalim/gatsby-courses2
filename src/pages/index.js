import React from "react";
import { graphql } from "gatsby";

export default function Home({ data }) {
  return (
    <div>
        <div>
            <h1>Full Stack Citation Courses</h1>
            <table>
                <thead>
                    <tr>
                        <th>Subject</th>
                        <th>Course</th>
                        <th>Title</th>
                    </tr>
                </thead>
                <tbody>{
                    data.allMongodbCpsc2650Courses.edges.map(({ node }, index) => (
                        <tr key={index}>
                            <td>{node.subject}</td>
                            <td>{node.course}</td>
                            <td><a href={'/course/' + node.subject + '/' + node.course}>{node.title}</a></td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>

        <div>
            <h1>List of Instructors</h1>
            <ul>
                {
                    data.allMongodbCpsc2650Instructors.edges.map(({ node }, index) => (
                        <li>
                            <a href={'/instructor/' + node.name + '/' + node.office}>{node.name}</a>
                        </li>
                    ))}
            </ul>
        </div>
    </div>
  )
}
  

export const query = graphql`
query MyQuery {
  allMongodbCpsc2650Courses {
    edges {
      node {
        subject
        course
        title
      }
    }
  }
  allMongodbCpsc2650Instructors {
    edges {
      node {
        name
        office
      }
    }
  }
}


`