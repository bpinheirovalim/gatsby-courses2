import React from "react"
import { graphql } from "gatsby";

export default function Instructor({ data }) {
  const instructor = data.mongodbCpsc2650Instructors;
  return (
    <div>
      <h1>{instructor.name}</h1>
      <table>
        <tr>
            <th>Name</th>
            <td>{instructor.name}</td>
        </tr>
        <tr>
            <th>Title</th>
            <td>{instructor.title}</td>
        </tr>
        <tr>
            <th>Office</th>
            <td>{instructor.office}</td>
        </tr>
    
        <tr>
            <th>Phone</th>
            <td>{instructor.phone}</td>
        </tr>
        
    </table>
    </div>
  );
}



export const query = graphql`
  query($slug: String!) {
    mongodbCpsc2650Instructors(fields: { slug: { eq: $slug } }) {
      name
      title
      office
      phone
    }
  }
`