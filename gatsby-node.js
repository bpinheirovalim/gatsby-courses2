const path = require(`path`);

exports.onCreateNode = ({ node, actions }) => {
  const { createNodeField } = actions;
  if (node.internal.type === `mongodbCpsc2650Courses`) {
    createNodeField({
        node,
        name: `slug`,
        value: `/course/${node.subject}/${node.course}/`,
    });
  }
  else if(node.internal.type === `mongodbCpsc2650Instructors`){
    createNodeField({
        node,
        name: `slug`,
        value: `/instructor/${node.name}/${node.office}/`,
    });
  }
}




exports.createPages = async ({ graphql, actions }) => {
  // **Note:** The graphql function call returns a Promise
  // see: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise for more info
  const { createPage } = actions;
  const result = await graphql(`
    {
        allMongodbCpsc2650Courses {
            edges {
            node {
                fields {
                slug
                }
            }
            }
        }
        allMongodbCpsc2650Instructors {
            edges {
            node {
                fields {
                slug
                }
            }
            }
        }
    }
  `);

  result.data.allMongodbCpsc2650Courses.edges.forEach(({ node }) => {
    createPage({
      path: node.fields.slug,
      component: path.resolve(`./src/templates/course.js`),
      context: {
        // Data passed to context is available
        // in page queries as GraphQL variables.
        slug: node.fields.slug,
      },
    })
  });
  result.data.allMongodbCpsc2650Instructors.edges.forEach(({ node }) => {
    createPage({
      path: node.fields.slug,
      component: path.resolve(`./src/templates/instructor.js`),
      context: {
        // Data passed to context is available
        // in page queries as GraphQL variables.
        slug: node.fields.slug,
      },
    })
  });
}